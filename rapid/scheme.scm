;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define (run-scheme)
  (parameterize ((version-etc-copyright
		  "Copyright © 2017 Marc Nieper-Wißkirchen"))
    (receive (input library-vicinities)
	(args-fold (cdr (command-line))
		   (list
		    (option '(#\h "help") #f #f
			    (lambda (option name arg input library-vicinities)
			      (help (current-output-port))
			      (exit)))
		    (option '(#\v "version") #f #f
			    (lambda (option name arg input library-vicinities)
			      (version-etc "rapid-scheme" "Rapid Scheme" "0.1.0")
			      (flush-output-port)
			      (exit)))
		    (option '(#\I "include") #t #t
			    (lambda (option name arg input library-vicinities)
			      (values input
				      (cons (make-vicinity arg)
					    library-vicinities)))))
		   (lambda (option name arg input library-vicinities)
		     (error-message 0 "invalid-option ‘~a’" name)
		     (help (current-error-port))
		     (exit 1))
		   (lambda (operand input library-vicinities)
		     (values operand library-vicinities))
		   #f '())
      
      (unless input
	(error-message 0 "no input file given")
	(help (current-error-port))
	(exit 1))

      (with-exception-handler
	  (lambda (condition)
	    (cond
	     ((syntax-error-object? condition)
	      ;; TODO: Handle fatal errors, syntax errors, 
	      (write-string (syntax-error-object->string condition) (current-error-port))
	      (newline (current-error-port))
	      (flush-output-port (current-error-port))
	      (exit 1))
	     (else
	      (raise-continuable condition))))
	(lambda ()
	  (let ((program
		 (read-file #f input #f)))
	    (let ((runtime-environment
		   (make-runtime-environment library-vicinities
					     feature-identifiers)))
	      (with-runtime-environment runtime-environment
		(lambda ()
		  (eval-program program))))))))))

(define (help port)
  (write-string
   (format "Usage: ~a [OPTION] file\n  \
              -I, --include prepends a library search directory\n  \
              -h, --help    display this help and exit\n  \
              -v, --version output version information and exit\n"
	   (car (command-line)))
   port)
  (newline port)
  (emit-bug-reporting-address "marc@rapid-scheme.org" port)
  (flush-output-port port))

(define feature-identifiers
  '(r7rs rapid-scheme rapid-scheme-0.1))
